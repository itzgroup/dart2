import 'dart:io';

void main(List<String> args) {
  String? input = stdin.readLineSync()!;
  List<String> words = input.toLowerCase().split(' ');
  var sum = {};
  for (var word in words) {
    if (!sum.containsKey(word)) {
      sum[word] = 1;
    } else {
      sum[word] += 1;
    }
  }

  print(sum);
}
